package com.test.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Class for running spring-boot application
 */
@SpringBootApplication
public class TestApplication
{

  /**
   * Main method for running spring-boot application
   * @param args from command line
   */

  public static void main( String[] args )
  {
    SpringApplication.run( TestApplication.class, args );
  }
}
