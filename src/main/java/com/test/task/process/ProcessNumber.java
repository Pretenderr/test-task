package com.test.task.process;

import com.test.task.model.CallPojo;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.swing.text.MaskFormatter;
import java.text.ParseException;

import static com.test.task.utils.CallConstants.*;

/**
 * Camel processor for processing phone number
 */

@Component
public class ProcessNumber
  implements Processor
{

  private static final Logger LOG = LoggerFactory.getLogger( ProcessNumber.class );

  /**
   * Camel processor method for processing phone number
   * exchange should have body type of CallPojo.class
   * setup body out as LASTNAME_FIRSTNAME String
   * setup FILE_NAME as header for camel-file component
   */

  @Override
  public void process( Exchange exchange )
    throws Exception
  {
    CallPojo body = exchange.getIn().getBody( CallPojo.class );
    String fileName = ( body.getLastName() + "_" + body.getFirstName() ).toUpperCase() + ".txt";
    exchange.getOut().setBody( formatNumber( body.getTelephoneNumber() ) + "\n" );
    exchange.getOut().setHeader( Exchange.FILE_NAME, fileName );
  }

  /**
   * Return formatted phone number
   *
   * @param number unformatted phone number
   * @return formatted phone number
   */

  private String formatNumber( String number )
  {
    String phoneNumber = number.replaceAll( "[^0-9]", "" );
    if ( phoneNumber.startsWith( DEFAULT_PLUS_REPLACER ) ) {
      phoneNumber = phoneNumber.replaceFirst( DEFAULT_PLUS_REPLACER, "" );
    }

    if ( phoneNumber.length() == 9 ) {
      phoneNumber = DEFAULT_PHONE_COUNTRY + phoneNumber;
    }

    String line = null;
    try {
      MaskFormatter maskFormatter = new MaskFormatter( PHONE_MASK );
      maskFormatter.setValueContainsLiteralCharacters( false );
      line = maskFormatter.valueToString( phoneNumber );
    } catch( ParseException e ) {
      LOG.error( e.getMessage() );
    }

    return line;
  }
}
