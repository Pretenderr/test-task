package com.test.task.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.test.task.utils.CallConstants.SIMPLE_DATE_FORMAT;

/**
 * Camel processor for processing time stamp
 */

@Component
public class ProcessTime
  implements Processor
{

  private static final SimpleDateFormat sdf = new SimpleDateFormat( SIMPLE_DATE_FORMAT );

  /**
   * Camel processor method for processing time stamp
   * setup body as new Date() formatted by SIMPLE_DATE_FORMAT "HH:mm:ss"
   */

  @Override
  public void process( Exchange exchange )
    throws Exception
  {
    exchange.getOut().setBody( sdf.format( new Date() ) + "\n" );
    exchange.getOut().setHeader( Exchange.FILE_NAME, exchange.getIn().getHeader( Exchange.FILE_NAME ) );
  }

}
