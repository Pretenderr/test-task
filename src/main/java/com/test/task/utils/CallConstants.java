package com.test.task.utils;

/**
 * Class for all constants
 */

public final class CallConstants
{

  private CallConstants(){
  }

  public static final String PATTERN_FOR_NUMBER = "^\\+?[0-9- ()]*$";
  public static final String MESSAGE_FOR_NUMBER = "Wrong phone number";

  public static final String PHONE_MASK = "00### ### ### ###";
  public static final String DEFAULT_PHONE_COUNTRY = "420";
  public static final String DEFAULT_PLUS_REPLACER = "00";

  public static final String SIMPLE_DATE_FORMAT = "HH:mm:ss";

  public static final String APPLICATION_JSON = "application/json";
  public static final String INVALID_JSON_DATA = "Invalid json data";
  public static final String VALIDATION_FAIL = "Validation fail";
  public static final String UNEXPECTED_ERROR = "Unexpected error";
  public static final String SERVICE_NAME = "test-task";
  public static final int HTTP_500_CODE = 500;

}
