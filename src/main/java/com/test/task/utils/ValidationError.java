package com.test.task.utils;

import javax.validation.ConstraintViolation;
import java.util.HashSet;
import java.util.Set;

/**
 * Validation errors helper
 */

public class ValidationError
{
  private String className;
  private String propertyPath;
  private String errorMessage;

  /**
   * Converting from standard BeanValidationException violations to Set<ValidationError>
   *   @see  org.apache.camel.component.bean.validator.BeanValidationException
   */

  public static Set<ValidationError> fromViolations( Set violations )
  {
    Set<ValidationError> errors = new HashSet<>();

    for( Object o : violations ) {
      ConstraintViolation v = (ConstraintViolation)o;

      ValidationError error = new ValidationError();
      error.setClassName( v.getRootBeanClass().getSimpleName() );
      error.setErrorMessage( v.getMessage() );
      error.setPropertyPath( v.getPropertyPath().toString() );
      errors.add( error );
    }

    return errors;
  }

  /**
   * @return current className
   */

  public String getClassName()
  {
    return className;
  }

  /**
   * @param className Set up className
   */

  public void setClassName( String className )
  {
    this.className = className;
  }

  /**
   * @return current propertyPath
   */

  public String getPropertyPath()
  {
    return propertyPath;
  }

  /**
   * @param propertyPath Set up propertyPath
   */

  public void setPropertyPath( String propertyPath )
  {
    this.propertyPath = propertyPath;
  }

  /**
   * @return current errorMessage
   */

  public String getErrorMessage()
  {
    return errorMessage;
  }

  /**
   * @param errorMessage Set up errorMessage
   */

  public void setErrorMessage( String errorMessage )
  {
    this.errorMessage = errorMessage;
  }

  /**
   * @return String for loggers
   */

  @Override
  public String toString()
  {
    return "ValidationError{" +
      "className='" + className + '\'' +
      ", propertyPath='" + propertyPath + '\'' +
      ", errorMessage='" + errorMessage + '\'' +
      '}';
  }
}
