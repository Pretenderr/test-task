package com.test.task.route;

import com.fasterxml.jackson.core.JsonParseException;
import com.test.task.model.CallPojo;
import com.test.task.model.ResponseError;
import com.test.task.process.ProcessNumber;
import com.test.task.process.ProcessTime;
import com.test.task.utils.CallConstants;
import com.test.task.utils.ValidationError;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static com.test.task.utils.CallConstants.*;

/**
 * Initial route for incoming http rest requests.
 */

@Component
public class MainRoute
  extends RouteBuilder
{

  /**
   * Method allow configure initial camel route, includes global exception handlers and REST DSL for rests
   */

  @Override
  public void configure()
    throws Exception
  {
    onException( Exception.class ).handled( true )
      .setHeader( Exchange.HTTP_RESPONSE_CODE, constant( HTTP_500_CODE ) )
      .setHeader( Exchange.CONTENT_TYPE, constant( APPLICATION_JSON ) )
      .process( exchange -> {
        ResponseError responseError = new ResponseError();
        responseError.setCode( String.valueOf( HTTP_500_CODE ) );
        responseError.setSourceService( SERVICE_NAME );
        responseError.setMessageError( UNEXPECTED_ERROR );
        exchange.getOut().setBody( responseError );
        exchange.getOut().setFault(true);
      } );

    onException( JsonParseException.class ).handled( true )
      .process( exchange -> {
        ResponseError responseError = new ResponseError();
        responseError.setCode( String.valueOf( HTTP_500_CODE ) );
        responseError.setSourceService( SERVICE_NAME );
        responseError.setMessageError( INVALID_JSON_DATA );
        exchange.getOut().setHeader( Exchange.HTTP_RESPONSE_CODE, constant( HTTP_500_CODE ) );
        exchange.getOut().setHeader( Exchange.CONTENT_TYPE, constant( APPLICATION_JSON ) );
        exchange.getOut().setBody( responseError );
        exchange.getOut().setFault(true);
      } );

    onException( BeanValidationException.class ).handled( true )
      .process( exchange -> {
        BeanValidationException cause = exchange.getProperty(
          Exchange.EXCEPTION_CAUGHT, BeanValidationException.class );
        Set<ConstraintViolation<Object>> violations = cause.getConstraintViolations();
        Set<ValidationError> errors = ValidationError.fromViolations( violations );
        ResponseError responseError = new ResponseError();
        responseError.setCode( String.valueOf( HTTP_500_CODE ) );
        responseError.setErrors( errors );
        responseError.setSourceService( SERVICE_NAME );
        responseError.setMessageError( VALIDATION_FAIL );
        exchange.getOut().setHeader( Exchange.HTTP_RESPONSE_CODE, constant( HTTP_500_CODE ) );
        exchange.getOut().setHeader( Exchange.CONTENT_TYPE, constant( APPLICATION_JSON ) );
        exchange.getOut().setBody( responseError );
        exchange.getOut().setFault(true);
      } ).end();

    restConfiguration().component( "restlet" ).bindingMode( RestBindingMode.json )
      .dataFormatProperty( "json.in.disableFeatures", "FAIL_ON_UNKNOWN_PROPERTIES" )
      .dataFormatProperty( "prettyPrint", "true" )
      .contextPath( "/" ).port( 8080 )
      .apiContextPath( "/api-doc" )
      .apiProperty( "api.title", "User API" ).apiProperty( "api.version", "1.2.3" )
      .apiProperty( "host", "localhost:8080" )
      .apiProperty( "cors", "true" );


    rest( "/user" ).description( "Call rest utils" ).consumes( APPLICATION_JSON ).produces( APPLICATION_JSON )
      .post( "/newcall" ).type( CallPojo.class )
      .route()
        .to( "bean-validator://validateCall" )
        .process( new ProcessNumber() )
        .to( "file://target/test-reports?fileExist=ignore" )
        .process( new ProcessTime() )
        .to( "file://target/test-reports?fileExist=Append" )
      .end()
      .transform().constant( null )
      .endRest();
  }
}