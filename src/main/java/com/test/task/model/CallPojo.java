package com.test.task.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.test.task.utils.CallConstants.MESSAGE_FOR_NUMBER;
import static com.test.task.utils.CallConstants.PATTERN_FOR_NUMBER;

/**
 * CallPojo class for rests, include validate
 */

@ApiModel(description = "Call POJO object ...")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CallPojo
{

  @Size(max = 30)
  private String firstName;

  @NotNull
  @Size(max = 30)
  private String lastName;

  @NotNull
  @Pattern(regexp = PATTERN_FOR_NUMBER, message = MESSAGE_FOR_NUMBER)
  private String telephoneNumber;

  /**
   * @return current firstName
   */

  public String getFirstName()
  {
    return firstName;
  }

  /**
   * @param firstName firstName to set @Size(max = 30)
   */

  public void setFirstName( String firstName )
  {
    this.firstName = firstName;
  }

  /**
   * @return current lastName
   */

  public String getLastName()
  {
    return lastName;
  }

  /**
   * @param lastName lastName to set @NotNull @Size(max = 30)
   */

  public void setLastName( String lastName )
  {
    this.lastName = lastName;
  }

  /**
   * @return current telephoneNumber
   */
  public String getTelephoneNumber()
  {
    return telephoneNumber;
  }

  /**
   * @param telephoneNumber lastName to set @NotNull use pattern "^\\+?[0-9- ()]*$"
   */

  public void setTelephoneNumber( String telephoneNumber )
  {
    this.telephoneNumber = telephoneNumber;
  }

}
