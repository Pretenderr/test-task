package com.test.task.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.test.task.utils.ValidationError;
import io.swagger.annotations.ApiModel;

import java.util.Set;

/**
 * ResponseError class for rests error responses
 */

@ApiModel(description = "ResponseError object ...")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseError
{

  private String code;
  private String messageError;
  private Set<ValidationError> errors;
  private String sourceService;

  public ResponseError()
  {
  }

  public ResponseError( String code, String messageError, Set<ValidationError> errors, String sourceService )
  {
    this.code = code;
    this.messageError = messageError;
    this.errors = errors;
    this.sourceService = sourceService;
  }

  /**
   * @return current code
   */

  public String getCode()
  {
    return code;
  }

  /**
   * @param code code to set
   */

  public void setCode( String code )
  {
    this.code = code;
  }

  /**
   * @return current Set<ValidationError> errors
   */

  public Set<ValidationError> getErrors()
  {
    return errors;
  }

  /**
   * @param errors Set<ValidationError> includes errors after validate
   */

  public void setErrors( Set<ValidationError> errors )
  {
    this.errors = errors;
  }

  /**
   * @return current sourceService
   */

  public String getSourceService()
  {
    return sourceService;
  }

  /**
   * @param sourceService sourceService to set. Use for setup current service name
   */

  public void setSourceService( String sourceService )
  {
    this.sourceService = sourceService;
  }

  /**
   * @return current messageError
   */

  public String getMessageError()
  {
    return messageError;
  }

  /**
   * @param messageError messageError to set. Use for notify user friendly message when something wrong.
   */

  public void setMessageError( String messageError )
  {
    this.messageError = messageError;
  }
}
