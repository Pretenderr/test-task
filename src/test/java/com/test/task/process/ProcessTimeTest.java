package com.test.task.process;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.testng.annotations.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.testng.Assert.*;

public class ProcessTimeTest
{

  private ProcessTime processTime = new ProcessTime();
  private final CamelContext context = new DefaultCamelContext();
  private final Exchange exchange = new DefaultExchange( context );

  @Test
  public void testProcess()
    throws Exception
  {
    exchange.getIn().setHeader( Exchange.FILE_NAME, "test" );
    processTime.process( exchange );

    assertNotNull( exchange.getOut().getBody() );
    assertEquals(  exchange.getOut().getHeader( Exchange.FILE_NAME ), "test" );
    assertTrue( exchange.getOut().getBody().toString().endsWith( "\n" ) );
  }

}