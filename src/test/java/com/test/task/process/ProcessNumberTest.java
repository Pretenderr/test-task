package com.test.task.process;

import com.test.task.model.CallPojo;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.test.task.utils.CallConstants.DEFAULT_PLUS_REPLACER;
import static com.test.task.utils.CallConstants.PHONE_MASK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProcessNumberTest
{

  private ProcessNumber processNumber = new ProcessNumber();
  private final CamelContext context = new DefaultCamelContext();
  private final Exchange exchange = new DefaultExchange( context );

  @DataProvider(name = "test1")
  public static Object[][] primeNumbers()
  {
    return new Object[][] { { "+(420) 111 222 333" }, { "+(420)-111222333" }, { "+420111222333" }, { "00420111222333" },
      { "(111) 222 (333)" }, { "123456789" }, {"+(375) 295264156"}
    };
  }

  @Test(dataProvider = "test1")
  public void testProcess( String number )
    throws Exception
  {
    CallPojo callPojo = new CallPojo();
    callPojo.setTelephoneNumber( number );
    exchange.getIn().setBody( callPojo );
    processNumber.process( exchange );

    assertTrue(exchange.getOut().getBody().toString().startsWith( DEFAULT_PLUS_REPLACER ));
    assertEquals( exchange.getOut().getBody().toString().length(), (PHONE_MASK + "\n").length() );
    assertTrue( exchange.getOut().getBody().toString().endsWith( "\n" ) );
  }

}