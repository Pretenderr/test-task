# Technology Involved
Java 1.8 - http://docs.oracle.com/javase/8/

Spring Boot 1.4.0 - http://projects.spring.io/spring-boot/

Apache Camel 2.17.3 - http://camel.apache.org/

Spring Framework 4.3.2.RELEASE - http://projects.spring.io/spring-framework/

Hibernate Validator 5.2.4.Final - http://hibernate.org/validator/

TestNG 6.9.12 - http://testng.org/doc/index.html

# Test task run
```
$ mvn spring-boot:run
```
or
```
$ mvn clean install
$ ./target/test-task-0.0.1-SNAPSHOT.jar
```

# Test examples
## Example success 
curl:
```
$ curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "firstName": "Dzmitry","lastName": "Hancharou","telephoneNumber": "+(375) 295264156" }' 'http://localhost:8080/user/newcall' -v
Note: Unnecessary use of -X or --request, POST is already inferred.
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> POST /user/newcall HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.47.0
> Content-Type: application/json
> Accept: application/json
> Content-Length: 88
> 
* upload completely sent off: 88 out of 88 bytes
< HTTP/1.1 200 OK
< Content-Length: 0
< Content-Type: application/json
< Connection: keep-alive
< 
* Connection #0 to host localhost left intact
```

Result:
File created:
```
target/test-reports/HANCHAROU_DZMITRY.txt
```
In file:
```
00375 295 264 156
14:21:37
14:24:55
14:25:03
14:26:59
14:28:31
14:30:02
14:30:45
```

## Example fail validation:
```
$ curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "firstName": "Dzmtry","lastName": "Hancharou","telephoneNumber": "+(375)X295264156" }' 'http://localhost:8080/user/newcall' -v
Note: Unnecessary use of -X or --request, POST is already inferred.
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> POST /user/newcall HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.47.0
> Content-Type: application/json
> Accept: application/json
> Content-Length: 88
> 
* upload completely sent off: 88 out of 88 bytes
< HTTP/1.1 500 Internal Server Error
< Server: Restlet-Framework/2.3.6
< Date: Fri, 09 Sep 2016 12:11:34 GMT
< Content-type: constant{application/json}
< Content-length: 227
< 
{
  "code" : "500",
  "messageError" : "Validation fail",
  "errors" : [ {
    "className" : "CallPojo",
    "propertyPath" : "telephoneNumber",
    "errorMessage" : "Wrong phone number"
  } ],
  "sourceService" : "test-task"
* Connection #0 to host localhost left intact
}
```